@extends('layouts.app')
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/')}}jssocials.css" />
{{--    <link rel="stylesheet" type="text/css" href="jssocials-theme-flat.css" />--}}
@endpush
@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="row justify-content-center">

            <!-- Post Content Column -->
            <div class="col-lg-10">

                <!-- Title -->
                <h1 class="mt-4 font-weight-bold shadow">{{$post->title}}</h1>

                <hr>

                <!-- Date/Time -->
                <p>Posted on {{$post->created_at}}</p>

                <hr>

                <!-- Preview Image -->
                <video class="videoDetails" src="{{url('/storage/'.$post->file)}}" controls></video>

                <hr>



                <!-- Post Content -->
                <p class="lead">{{$post->description}}</p>

                <hr>


            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->



@stop
@push('js')
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5efff86a1a7e304d"></script>
@endpush
