@extends('layouts.app')
@push('css')

    <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/jssocials/dist/jssocials.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/jssocials/dist/jssocials-theme-classic.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/jssocials/dist/jssocials-theme-minima.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/jssocials/dist/jssocials-theme-plain.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/jssocials/dist/jssocials-theme-flat.css')}}" />


@endpush
@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="row justify-content-center">

            <!-- Post Content Column -->
            <div class="col-lg-10">

                <!-- Title -->
                <h1 class="mt-4 shadow font-weight-bold">{{$post->title}}</h1>

                <hr>

                <!-- Date/Time -->
                <p>Posted on {{$post->created_at}}</p>

                <hr>
<div class="card">
    <img src="{{url('/storage/'.$post->file)}}" alt="">
</div>
                <!-- Preview Image -->

{{--                <video class="videoDetails" src="{{asset('assets/frontend/demo.mp4')}}" controls></video>--}}

                <hr>

                <!-- Post Content -->
                <p class="lead">{{$post->description}}</p>



                <hr>



            </div>

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->


@stop
@push('js')
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5efff86a1a7e304d"></script>
    <script src=" https://code.jquery.com/jquery-2.1.0.min.js"></script>
    <script src="{{asset('assets/frontend/jssocials/src/jssocials.js')}}"></script>
    <script src="{{asset('assets/frontend/jssocials/src/jssocials.shares.js')}}"></script>
    <script>
        $("#share").jsSocials({
            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
        });
    </script>
@endpush
