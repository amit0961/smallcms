@extends('layouts.app')
@push('css')
    <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/fontawesome.min.css">

@endpush
@section('content')
<div class="container">
    <div class="row">
        <!-- Blog Entries Column -->
        <div class="col-md-8">
            <h1 class="my-4 font-weight-bold shadow">Video-Post-Section</h1>
            <div class="row">
                @foreach($posts as $post)
                    @if($post->category_id==1 && $post->status==1)
                        <div class="col-md-6">
                            <a href="{{route('videoDetails', $post->id)}}">
{{--                                <iframe  src="" frameborder="0"></iframe>--}}
                                <video src="{{url('/storage/'.$post->file)}}"> </video>
                                <p>{{ Str::limit($post->title,50) }}<i style="font-size:20px; float: right" class="far fa-play-circle"></i></p>

                                <hr>
                            </a>
                        </div>
                    @endif
                @endforeach

            </div>
        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">
            <!-- Categories Widget -->
            <h1 class="my-4 font-weight-bold shadow">Blog-Post Section</h1>

                @foreach($posts as $post)
                    @if($post->category_id==2 && $post->status==1)
                        <div class="card mt-2">
                        <img height="200px"  src="{{url('/storage/'.$post->file)}}" class="img-responsive">
                        <div class="card-body">
                            <h3 class="card-title font-weight-bold ">{{Str::limit($post->title,25)}}</h3>
                            <p class="card-text">{{Str::limit($post->description,60)}}</p>
                                <a href="{{route('postDetails',$post->id)}}" class="btn btn-primary">Read More &rarr;</a>
                        </div>
                        </div>
                    @endif
                @endforeach

        </div>
    </div>
    <!-- /.row -->
</div>
@stop
    @push('js')
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    @endpush


