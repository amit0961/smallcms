<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        $posts=Post::all();
        return view('frontend.index',compact('posts'));
    }
    public function pDetails($id){
        $post = Post::find($id);
        return view('frontend.postDetails',compact('post'));
    }
    public function vDetails($id){
       $post = Post::find($id);
        return view('frontend.videoDetails',compact('post'));
    }
}
